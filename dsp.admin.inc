<?php

/**
 * @file
 * Admin page callback file for the date specific permissions module.
 */

/**
 * Page callback: Generates the appropriate node with mood listing.
 *
 * @return string
 *   A renderable form array for the respective request.
 */
function dsp_listing() {
  $header = array(
    array('data' => t('ID'), 'field' => 'id', 'sort' => 'asc'),
    array('data' => t('Permission'), 'field' => 'permission'),
    array('data' => t('Role'), 'field' => 'role'),
    array('data' => t('Grant on'), 'field' => 'grant_date'),
    array('data' => t('Revoke on'), 'field' => 'revoke_date'),
    array('data' => t('Delete'), 'field' => 'delete'),
  );
  $rows = array();
  $role_names = user_roles();
  $query = db_select('date_specific_permission', 'dsp')
    ->fields('dsp', array(
      'id',
      'permission',
      'role',
      'grant_date',
      'revoke_date',
    ));
  $table_sort = $query->extend('TableSort')
    ->orderByHeader($header);
  $pager = $table_sort->extend('PagerDefault')
    ->limit(10);
  $results = $pager->execute();
  $i = 1;
  foreach ($results as $row) {
    $rows[] = array($i++,
      $row->permission,
      $role_names[$row->role],
      $row->grant_date ? $row->grant_date : '-',
      $row->revoke_date ? $row->revoke_date : '-',
      l(t('delete'), 'admin/date_specific_permission/' . $row->id . '/delete',
        array('query' => array('destination' => 'admin/dsp'))),
    );
  }
  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
  )) . theme('pager');
}

/**
 * Form constructor for the date specific permissions add form.
 *
 * @see dsp_add_submit()
 *
 * @ingroup forms
 */
function dsp_add($form, &$form_state) {
  $permissions = dsp_get_permissions();
  // Retrieve role names for columns.
  $role_names = user_roles();
  $form['permission'] = array(
    '#type' => 'select',
    '#title' => t('Select permission:'),
    '#options' => $permissions,
    '#required' => TRUE,
    '#description' => '<p>' . t('Select permission which you want to grant or revoke.') . '</p>',
  );
  $form['role'] = array(
    '#type' => 'select',
    '#title' => t('Select role:'),
    '#options' => $role_names,
    '#required' => TRUE,
    '#description' => '<p>' . t('Select a role to which you want the above selected permission to grant or revoke.') . '</p>',
  );
  $active = array(0 => t('Grant'), 1 => t('Revoke'));
  $form['option'] = array(
    '#type' => 'radios',
    '#title' => t('Select Option:'),
    '#default_value' => 0,
    '#options' => $active,
    '#required' => TRUE,
    '#description' => t('Select an option Grant / Revoke.'),
  );
  $form['grant_date'] = array(
    '#type' => 'date',
    '#title' => t('Grant date:'),
    '#required' => TRUE,
    '#description' => '<p>' . t('Select a date on which you need to apply grant permission on the selected role.') . '</p>',
    '#states' => array(
      'invisible' => array('input[name="option"]' => array('value' => '1')),
      'visible' => array('input[name="option"]' => array('value' => '0')),
    ),
  );
  $form['revoke_date'] = array(
    '#type' => 'date',
    '#title' => t('Revoke date:'),
    '#required' => TRUE,
    '#description' => '<p>' . t('Select a date on which you need to apply revoke permission on the selected role.') . '</p>',
    '#states' => array(
      'invisible' => array('input[name="option"]' => array('value' => '0')),
      'visible' => array('input[name="option"]' => array('value' => '1')),
    ),
  );
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Form submission handler for dsp_add().
 *
 * @see dsp_add()
 */
function dsp_add_submit($form, &$form_state) {
  global $user;
  $values = $form_state['values'];
  $row = new stdclass();
  $row->permission = $values['permission'];
  $row->role = $values['role'];
  $row->option = $values['option'];
  if ($row->option) {
    $row->revoke_date = implode('/', $values['revoke_date']);
  }
  else {
    $row->grant_date = implode('/', $values['grant_date']);
  }
  $row->uid = $user->uid;
  $row->created = REQUEST_TIME;
  drupal_write_record('date_specific_permission', $row);
  drupal_set_message(t('Your date specific permission has been saved successfully!'));
}

/**
 * Function to confirm delete action on date specific permission.
 *
 * @param int $id
 *   Date specific permission id to delete.
 *
 * @see dsp_delete_confirm_submit()
 */
function dsp_delete_confirm($form, &$form_state, $id) {
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );
  return confirm_form($form,
    t('Are you sure you want to delete this date specific permission?'), '',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'));
}

/**
 * Form submission handler for dsp_delete_confirm().
 *
 * @see dsp_delete_confirm()
 */
function dsp_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    dsp_delete($form_state['values']['id']);
    drupal_set_message(t('Date specific permission has been deleted successfully.'));
  }
}

/**
 * Form constructor for the date specific permissions update manually form.
 *
 * @see dsp_update_permissions_manually_form_submit()
 *
 * @ingroup forms
 */
function dsp_update_permissions_manually_form($form, &$form_state) {
  $form['form_description'] = array(
    '#markup' => '<p>' . t('Please click the run update button to update your date specific permissions manually.') . '</p>' . '<p>' . 'By clicking on this button all the date specific permissions scheduled today will be updated.' . '</p>',
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Run Update'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/dsp'),
  );
  return $form;
}

/**
 * Form submission handler for dsp_update_permissions_manually_form().
 *
 * @see dsp_update_permissions_manually_form()
 */
function dsp_update_permissions_manually_form_submit($form, &$form_state) {
  dsp_update_permissions();
  drupal_set_message(t('Permissions have been updated successfully.'));
  $form_state['redirect'] = 'admin/dsp';
}
