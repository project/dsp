CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration


INTRODUCTION
------------

Current Maintainer: Viswanath Polaki <polaki_2005@yahoo.com>

This module allows site admin to grant and revoke permissions based on date.
Site admin must select which permission to be provided / revoked to which role
and for a specific date interval.

Usage example:
If there is a requirement where site admin wants to grant a permission for a
specific role for lets say date X to date Y. So site admin can grant a
permission from date X and site admin can revoke a granted permission on
date Y for a specific role.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
Site admins can manage date specific permissions via admin panel
For viewing list of date specific permissions
URL: <yourdomain.com>/admin/dsp
